/**
 * Library to support updating Drupal items via CTools without page refresh.
 */
(function ($) {
  // Location to register callbacks for node operations.
  Drupal.notify_node = Drupal.notify_node || {};
})(jQuery);