/**
 * Library to support updating Drupal items via CTools without page refresh.
 */
(function ($) {
  /**
   * Similar to Drupal.behaviors, call all functions that register to be notified
   * about node updates.
   */
  Drupal.CTools.AJAX.commands.notify_node = function(node, type, op) {
    // Execute all of them.
    jQuery.each(Drupal.notify_node, function() {
      this(node, type, op);
    });
  }
})(jQuery);